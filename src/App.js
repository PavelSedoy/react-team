import React from 'react';
import './App.css';

const App = () => <PersonList />;

const PersonList = () => {
  const people = [
      {
        img: 'https://image.ibb.co/eTomxd/8764.png',
        name: 'Mohamed Sae',
        job: 'UI / UX',
      },
      {
        img: 'https://image.ibb.co/b868Sd/7878.png',
        name: 'Aya Mostafa',
        job: 'Seo',
      },
      {
        img: 'https://image.ibb.co/c2ymxd/98977.png',
        name: 'Osama Elwan',
        job: 'Web Design',
      },
  ]

  return (
      <div className="wrapper">
          <Person person={people[0]} >
            Lorem ipsum dolor sit amet, consectetuer adipiscing Aenean commodo1
          </Person>
          <Person person={people[1]} >
            Lorem ipsum dolor sit amet, consectetuer adipiscing Aenean commodo2
          </Person>
          <Person person={people[2]} >
            Lorem ipsum dolor sit amet, consectetuer adipiscing Aenean commodo3
          </Person>
      </div>
  )
}

const Person = props => {
  const { img, name, job } = props.person;
  const { children } = props;
  const url = img;

  return (<div className="team-block">
      <svg className="svg" width="370px" height="156px">
          <path fill-rule="evenodd"  fill="rgb(222, 87, 73)"
                d="M370 ,41.770 C370.000,33.940 358.673,27.520 344.797,27.501 L310.718,27.456 C296.842,27.438 276.639,23.409 265.822,18.503 C265.822,18.503 225.256,0.106 185.008,0.053 C144.760,-0.001 104.042,18.290 104.042,18.290 C93.186,23.167 72.950,27.140 59.072,27.123 L25.625,27.078 C11.748,27.059 0.368,33.451 0.334,41.280 L0.031,113.881 C-0.000,121.711 11.326,128.132 25.204,128.150 L58.149,128.193 C72.027,128.211 92.197,132.262 102.976,137.195 C102.976,137.195 143.832,155.892 184.359,155.946 C224.887,155.999 265.897,137.410 265.897,137.410 C276.716,132.507 296.923,128.510 310.799,128.528 L344.376,128.571 C358.252,128.591 369.634,122.200 369.667,114.370 L369.969,41.770 Z"/>
      </svg>
      <img className="picturer" src={url} alt="picturer" />
        <div className="bc">
          <h3 className="desc-title">{name} {job}</h3>
          <p className="desc">{children}</p>
        </div>
    </div>)
}

export default App;